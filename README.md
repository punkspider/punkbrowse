punk-spider-chrome-ext
======================

Unofficial Chrome extension for PunkSpider (http://punkspider.hyperiongray.com).

PunkSPIDER is an open-source web application fuzzer that spiders the internet and fuzzes all of the websites
that it crawls (subject to robots.txt, of course). This extension checks whether the site you are currently on
has been fuzzed by PunkSPIDER.

If PunkSPIDER has fuzzed the site, you'll see the PunkSPIDER logo in the URL bar. If PunkSPIDER didn't find any
vulnerabilities, then the logo will display a green checkmark. If PunkSPIDER did find vulnerabilities, then the
logo displays a red X. You can click on the logo for more information.

PRIVACY NOTICE: Please note that this extension sends the domain of the site you are currently viewing (but not
the full URL) to PunkSPIDER for processing. This information may appear in PunkSPIDER logs and also may be
observable by anybody who is eavesdropping on your connection. This extension is deactivated automatically when
Chrome is in "Incognito" mode.
