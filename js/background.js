/*
 * CONSTANTS
 */
PUNK_SPIDER_HOST = "punkspider.hyperiongray.com";
PUNK_SPIDER_PATH = "/service/search/domain/";
PUNK_SPIDER_PAGE_SIZE = 100; // How many items to ask for per page of results.
PUNK_SPIDER_MAX_PAGES = 10; // The max number of pages to check before giving up.

PUNK_SPIDER_UNSCANNED_ICON = {
    "19": "images/spider19-plain.png",
    "38": "images/spider38-plain.png"
};
PUNK_SPIDER_WARNING_ICON = {
    "19": "images/spider19-red-x.png",
    "38": "images/spider38-red-x.png"
};
PUNK_SPIDER_CLEAN_ICON = {
    "19": "images/spider19-green-check.png",
    "38": "images/spider38-green-check.png"
};
/*
 * Construct a URL for getting search results from PunkSPIDER.
 */
function make_punk_spider_search_url(key, value, page) {
    var url_params_dict = {
        "searchkey": key,
        "searchvalue": value,
        "pagesize": PUNK_SPIDER_PAGE_SIZE,
        "pagenumber": page,
        "agent": "chrome-ext"
    };

    var url = "http://" + PUNK_SPIDER_HOST + PUNK_SPIDER_PATH + "?";

    var url_params_arr = Array();
    for (var url_param in url_params_dict) {
        url_params_arr.push(url_param + "=" + url_params_dict[url_param]);
    }

    url += url_params_arr.join("&");

    return url;
}

/*
 * Given a URL, return a prefix that contains only the protocol and host.
 *
 * E.g., given http://foobar.com:1234/baz/bat?q=1, return http://foobar.com:1234/
 */
function get_url_prefix(url) {
    var url_prefix_regex = /^[-\w]+:\/\/[^\/]+\//;
    var regex_result = url_prefix_regex.exec(url);

    if (regex_result.length >= 1) {
        var url_prefix = regex_result[0];
    } else {
        var url_prefix = null;
    }

    return url_prefix;
}

/*
 * Given a URL, return the hostname part of the URL.
 *
 * E.g., given http://foobar.com:1234/baz/bat?q=1, return foobar.com
 */
function get_host_from_url(url) {
    var host_regex = /^[-\w]+:\/\/([^:\/]+)/;
    var regex_result = host_regex.exec(url);

    if (regex_result.length >= 2) {
        var host = regex_result[1];
    } else {
        var host = null;
    }

    return host;
}

/*
 * Check PunkSPIDER to see if there are any known vulns for the
 * specified URL.
 *
 * If there are known vulns, then show the PunkSPIDER icon on the
 * specified tab.
 */
function query_punk_spider(url, tabId, page) {
    // If page is not specified, default to zero:
    var page = typeof page !== 'undefined' ? page : 1;

    if (url_in_cache(url, tabId)) {
        return;
    }

    // Send request:
    var host = get_host_from_url(url);
    var punkspider_query_url = make_punk_spider_search_url('url', host, page);
    console.log(punkspider_query_url);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", punkspider_query_url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            var response = JSON.parse(xhr.responseText);
            if ('data' in response && 'domainSummaryDTOs' in response.data) {
                var result_match = check_results(url, response.data.domainSummaryDTOs, tabId);

                // If nothing matches in the current page of results, see if there are more
                // pages that can be fetched.
                if (!result_match && response.data.numberOfPages > page && page <= PUNK_SPIDER_MAX_PAGES) {
                    query_punk_spider(url, tabId, page + 1);
                }
            } else {
                console.log("Unexpected response from PunkSPIDER.");
                console.log(response);
            }
        }
    }
    xhr.send();
}

/*
 * Look to see if the current URL is in the cache. If it is, update the UI and return true.
 */
function url_in_cache(url, tabId) {

    // Clean the cache once every minute.
    if ("last_cache_clean_date" in window.localStorage) {
        var last_cache_clean_date = new Date(window.localStorage["last_cache_clean_date"]);
        last_cache_clean_date.setMinutes(last_cache_clean_date.getMinutes() + 1);
        var now = new Date();

        if (last_cache_clean_date < now) {
            clean_cache();
        }
    } else {
        window.localStorage["last_cache_clean_date"] = new Date();
    }

    // Check whether this URL exists in the cache.
    if (url in window.localStorage) {
        var site = JSON.parse(window.localStorage[url]);

        console.log("Site found in cache")
        console.log(site)

        update_ui(site, tabId);

        return true;
    }

    update_ui_with_unscanned(site, tabId);
    
    return false;
}

/*
 * Iterate through all sites in the cache and remove any over 1 hour old.
 */
function clean_cache() {
    console.log("Cleaning cache");

    for (i in window.localStorage) {
        var item = window.localStorage[i];

        if (i == "last_cache_clean_date") {
            continue;
        } else {
            try {
                var site = JSON.parse(window.localStorage[i]);
                var expiration = new Date();
                expiration.setHours(expiration.getHours() - 1);
                cache_date = new Date(site.cache_date)

                if (cache_date < expiration) {
                    console.log("Purging " + i + " from cache.");
                    window.localStorage.removeItem(i);
                }
            } catch (err) {
                console.log("Unexpected cache key/value: " + i + ", " + window.localStorage[i]);
                window.localStorage.removeItem(i);
            }
        }
    }

    window.localStorage["last_cache_clean_date"] = new Date();
}

/*
 * Look through an array of sites returned from PunkSPIDER and see if any of the sites
 * match the given URL.
 *
 * When a match is found, the UI is updated and the function returns true. Otherwise,
 * the UI is not modified and the function returns false.
 */
function check_results(url, sites, tabId) {

    for (var site_index in sites) {
        var site = sites[site_index];

        // Check for matching URLs with and without trailing slash.
        if (site.url == url || site.url == url.substring(0, url.length-1)) {
            console.log("Match found");
            console.log(site);

            update_ui(site, tabId);

            // Update cache
            site.cache_date = new Date();
            window.localStorage[url] = JSON.stringify(site);

            return true;
        }
    }

    update_ui_with_unscanned(site, tabId);
    
    return false;
}  
/*
 * Update the UI to display the appropriate PunkSPIDER icon.
 */ 
function update_ui(site, tabId) {
    var total_vulns = site.bsqli + site.sqli + site.xss;

    if (total_vulns > 0) {
        var title = "Warning: PunkSPIDER found vulnerabilities on this site!";
        var path = PUNK_SPIDER_WARNING_ICON;
        var popup = "html/warning.html";
    } else {
        var title = "Phew! PunkSPIDER did not find vulnerabilities on this site.";
        var path = PUNK_SPIDER_CLEAN_ICON;
        var popup = "html/clean.html";
    }

    chrome.pageAction.setTitle({"tabId": tabId, "title": title});
    chrome.pageAction.setIcon({"tabId": tabId, "path": path});
    chrome.pageAction.setPopup({"tabId": tabId, "popup": popup});

    chrome.pageAction.show(tabId);

    // g_site is intentionally defined in the global scope so that the popup can get to it later!
    g_site = site;
}

function update_ui_with_unscanned(site, tabId) {
        var title = "This site has not been scanned by PunkSPIDER";
        var path = PUNK_SPIDER_UNSCANNED_ICON;
        var popup = "html/unscanned.html";

    chrome.pageAction.setTitle({"tabId": tabId, "title": title});
    chrome.pageAction.setIcon({"tabId": tabId, "path": path});
    chrome.pageAction.setPopup({"tabId": tabId, "popup": popup});

    chrome.pageAction.show(tabId);

    g_site = site;
}
/*
 * Add an event listener that will fire each time the user navigates to
 * a new URL.
 */
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status == "loading") {
        chrome.pageAction.hide(tabId);
        if (tab.url.substring(0,5) == "http:") {
            var url_prefix = get_url_prefix(tab.url);
            query_punk_spider(url_prefix, tabId);
        }
    }
});