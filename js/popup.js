window.onload = function () {
    var bg = chrome.extension.getBackgroundPage();
    var site = bg.g_site;

    if (site) {
        // Fill in number of vulns found into place holders.
        var ids_to_replace = ["bsqli", "sqli", "xss"];
        for (i in ids_to_replace) {
            var id_to_replace = ids_to_replace[i];

            var el = document.getElementById(id_to_replace);

            if (el) {
                el.innerHTML = site[id_to_replace];
            }
        }
    } else {
        console.log("No site found in background page.");
        console.log(bg);
    }
}
